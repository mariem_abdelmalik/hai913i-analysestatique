package grapheAppel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import visitor.MethodDeclarationVisitor;
import visitor.MethodInvocationVisitor;
import visitor.TypeDeclarationVisitor;


public class CallGraph {
	List<Node> nodes =  new ArrayList<Node>();
	List<Edge> edgs = new ArrayList<Edge>();
	
	public CallGraph (){
	}
	
	public CallGraph (ArrayList<Node> nodes,List<Edge> edgs){
		this.nodes=nodes;
		this.edgs=  edgs;
	}
	
	//initialisation de graph d'appel
	public void initCallGraph(List<CompilationUnit> compilationUnits) {
		
		for (CompilationUnit parse : compilationUnits) {
			
			TypeDeclarationVisitor visitor = new TypeDeclarationVisitor();
			
			parse.accept(visitor);
			
			for (TypeDeclaration type : visitor.getTypes()) {
				
				if (!type.isInterface()) {
					
					MethodDeclarationVisitor visitor2 = new MethodDeclarationVisitor();
					
					type.accept(visitor2);
					//pour chaque méthode nous cherchons l'ensemble des méthodes qu'elle appele par cette methode
					for (MethodDeclaration method : visitor2.getMethods()) {
						
						MethodInvocationVisitor visitor3 = new MethodInvocationVisitor();
						
						method.accept(visitor3);
						
						for (MethodInvocation methodInvocation : visitor3.getMethods()) {
							//nous avons choisir que le nom de la methode soit suivi du nom de son class.
							Node src = new Node(method.getName().toString()+"-"+type.getName().toString());
							
							Node dest = new Node(methodInvocation.getName().toString()+"-"+type.getName().toString());
							
							if (!this.getNodes().contains(src)) {
								
								this.getNodes().add(src);
								
							}
							
							if (!this.getNodes().contains(dest)) {
								
								this.getNodes().add(dest);
								
							}
													
							Edge edge = new Edge(src,dest);
							
							int i = edgeAlreadyExists(edge,this.getEdgs());
							
							if(i==-1) {

								this.edgs.add(edge);
								
							}
							
							else {
																
								this.getEdgs().add(edge);
								
							}
							
						}
					}
				}
			}
		}
	}
	
	//verification de l'existance d'edge dans la liste des edges.
	public int edgeAlreadyExists(Edge edge, List<Edge> edges){
		  
		  for (int i=0 ;i< edges.size() ;i++) {
			  
			  if(edge.getSrc().getName().equals(edges.get(i).getSrc().getName())  && edge.getDest().getName().equals(edges.get(i).getDest().getName())) {
				  
				  return i;
				  
			  }
		  }
		  
		  return -1;
		}
	
	public ArrayList<Node> getNodes() {
		return (ArrayList<Node>) nodes;
	}
	public void setNodes(ArrayList<Node> nodes) {
		this.nodes = nodes;
	}
	public List<Edge> getEdgs() {
		return edgs;
	}
	public void setEdgs(List<Edge> edgs) {
		this.edgs = edgs;
	}

	
	
}





