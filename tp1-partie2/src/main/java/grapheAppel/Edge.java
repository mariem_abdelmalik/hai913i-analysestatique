package grapheAppel;

public class Edge { 
	Node src, dest;
	
	public Edge(Node src, Node dest) { 
		this.src = src; this.dest = dest; 
		}

	public Node getSrc() {
		return src;
	}

	public void setSrc(Node src) {
		this.src = src;
	}

	public Node getDest() {
		return dest;
	}

	public void setDest(Node dest) {
		this.dest = dest;
	}
	
	@Override
	public boolean equals (Object object) {
		
		if (this.src.name==((Edge)object).src.name && this.dest.name==((Edge)object).dest.name) {
			return true;
		}
		return false;
		
	}
	
	
} 
