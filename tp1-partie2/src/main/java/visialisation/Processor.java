package visialisation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.eclipse.jdt.core.dom.SimpleName;

import calculStatistique.Parser;
import grapheAppel.CallGraph;
import grapheAppel.Edge;

public class Processor extends JFrame {
	public Processor() {
	}

	public void graphiqueInterface(String projectPath,String jrePath) throws IOException {

		
	//	final String projectPath = "/home/mariem/Documents/Master-montpellier/evolution-et-restruction-logicielle/eclipse/appTest";
		// final String projectSourcePath = projectPath + "/src";
	//	final String jrePath = "/home/mariem/.p2/pool/plugins/org.eclipse.justj.openjdk.hotspot.jre.full.linux.x86_64_18.0.2.v20220815-1350/jre";

		final Parser parser = new Parser(projectPath, jrePath);

		JFrame f1 = new JFrame("App");

		JButton CalculStatistique = new JButton("Calcul statistique");
		JButton callGraph = new JButton("Graphe d'appel");
		CalculStatistique.setBounds(100, 100, 300, 30);
		callGraph.setBounds(100, 150, 300, 30);
		f1.add(CalculStatistique);
		f1.add(callGraph);

		f1.setSize(500, 500);
		f1.setLayout(null);
		f1.setVisible(true);

		callGraph.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JFrame f = new JFrame("Graph d'appel");
				
				CallGraph callGraph = new CallGraph();
				
				callGraph.initCallGraph(parser.getCompilationUnits());
								
				String s = "";
				
				for (Edge edge:callGraph.getEdgs()) {
					
					s = s+edge.getSrc().getName()+  " --> "+edge.getDest().getName()+"<br>";
					
		

				}
				

				JLabel label = new JLabel("<html><div style='text-align: center;'>" +s+"</div></html>");
				
				label.setLocation(100, 100);
				label.setFont(new Font("Verdana", Font.PLAIN, 13));

				f.getContentPane().add(label,JLabel.CENTER);
				
				f.add(label);
				f.pack();
				f.setSize(500, 500);
				f.setLayout(null);
				f.setVisible(true);

				

			}
		});
		CalculStatistique.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JFrame f = new JFrame("Calcul statistique");

				JButton b1 = new JButton("Nombre de classes de l’app");
				JButton b2 = new JButton("Nombre de lignes de code de l’app");
				JButton b3 = new JButton("Nombre total de méthodes de l’app");
				JButton b4 = new JButton("Nombre total de packages de l’app");

				JButton b5 = new JButton("Nombre moyen de méthodes par classe");
				JButton b6 = new JButton("Nombre moyen de lignes de code par méthode");
				JButton b7 = new JButton("Nombre moyen d’attributs par classe");
				JButton b8 = new JButton("Les 10% des classes qui possèdent le plus grand nombre de méthodes");
				JButton b9 = new JButton("Les 10% des classes qui possèdent le plus grand nombre d’attributs");
				JButton b10 = new JButton("Les classes qui font partie en même temps des deux catégories précédentes");
				JButton b11 = new JButton("Les classes qui possèdent plus de X méthodes (la valeur de X est donnée)");
				JButton b12 = new JButton(
						"Les 10% des méthodes qui possèdent le plus grand nombre de lignes de code (par classe)");
				JButton b13 = new JButton(
						"Le nombre maximal de paramètres par rapport à toutes les méthodes de l’application");

				b1.setBounds(50, 100, 700, 30);
				b2.setBounds(900, 100, 700, 30);
				b3.setBounds(50, 150, 700, 30);
				b4.setBounds(900, 150, 700, 30);
				b5.setBounds(50, 200, 700, 30);
				b6.setBounds(900, 200, 700, 30);
				b7.setBounds(50, 250, 700, 30);
				b8.setBounds(900, 250, 700, 30);
				b9.setBounds(50, 300, 700, 30);
				b10.setBounds(900, 300, 700, 30);
				b11.setBounds(50, 350, 700, 30);
				b12.setBounds(900, 350, 700, 30);
				b13.setBounds(50, 400, 700, 30);

				f.add(b1);
				f.add(b2);
				f.add(b3);
				f.add(b4);
				f.add(b5);
				f.add(b6);
				f.add(b7);
				f.add(b8);
				f.add(b9);
				f.add(b10);
				f.add(b11);
				f.add(b12);
				f.add(b13);

				b1.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						JFrame frame = new JFrame("Calcul statistique");
						frame.setMinimumSize(new Dimension(400, 400));
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						JLabel lblText = new JLabel("Nombre de classe de l'app : " + parser.getNbOfClasses(),
								SwingConstants.CENTER);
						frame.getContentPane().add(lblText);

						frame.pack();
						frame.setVisible(true);

					}
				});

				b2.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						JFrame frame = new JFrame("Calcul statistique");
						frame.setMinimumSize(new Dimension(400, 400));
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						JLabel lblText = new JLabel("Nombre de lignes de code de l’app : " + parser.getNbOfLines(),
								SwingConstants.CENTER);
						frame.getContentPane().add(lblText);

						frame.pack();
						frame.setVisible(true);

					}
				});

				b3.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						JFrame frame = new JFrame("Calcul statistique");
						frame.setMinimumSize(new Dimension(400, 400));
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						JLabel lblText = new JLabel("Nombre total de méthodes de l’app : " + parser.getNbOfMethodes(),
								SwingConstants.CENTER);
						frame.getContentPane().add(lblText);

						frame.pack();
						frame.setVisible(true);

					}
				});

				b4.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						JFrame frame = new JFrame("Calcul statistique");
						frame.setMinimumSize(new Dimension(400, 400));
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						JLabel lblText = new JLabel("Nombre total de packages de l’app : " + parser.getNbOfPackages(),
								SwingConstants.CENTER);
						frame.getContentPane().add(lblText);

						frame.pack();
						frame.setVisible(true);

					}
				});

				b5.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						JFrame frame = new JFrame("Calcul statistique");
						frame.setMinimumSize(new Dimension(400, 400));
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						JLabel lblText = new JLabel("Nombre moyen de méthodes par classe : "
								+ parser.getNbOfMethodes() / parser.getNbOfClasses(), SwingConstants.CENTER);
						frame.getContentPane().add(lblText);

						frame.pack();
						frame.setVisible(true);

					}
				});
				b6.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						JFrame frame = new JFrame("Calcul statistique");
						frame.setMinimumSize(new Dimension(400, 400));
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						JLabel lblText = new JLabel(
								"Nombre moyen de lignes de code par méthode : " + parser.getAVGCodeLinesPerMethod(),
								SwingConstants.CENTER);
						frame.getContentPane().add(lblText);

						frame.pack();
						frame.setVisible(true);

					}
				});
				b6.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						JFrame frame = new JFrame("Calcul statistique");
						frame.setMinimumSize(new Dimension(400, 400));
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						JLabel lblText = new JLabel(
								"Nombre moyen de lignes de code par méthode : " + parser.getAVGCodeLinesPerMethod(),
								SwingConstants.CENTER);
						frame.getContentPane().add(lblText);

						frame.pack();
						frame.setVisible(true);

					}
				});
				b7.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						JFrame frame = new JFrame("Calcul statistique");
						frame.setMinimumSize(new Dimension(400, 400));
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						JLabel lblText = new JLabel("Nombre moyen d'attribut par classe : "
								+ parser.getNbOfAttributes() / parser.getNbOfClasses(), SwingConstants.CENTER);
						frame.getContentPane().add(lblText);

						frame.pack();
						frame.setVisible(true);

					}
				});
				b8.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						JFrame frame = new JFrame("Calcul statistique");
						frame.setMinimumSize(new Dimension(400, 400));
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						JLabel lblText = new JLabel(
								"Les 10% des classes qui possèdent le plus grand nombre de méthodes : "
										+ parser.getTenPercentOfClassesHaveTheMostMethods(),
								SwingConstants.CENTER);
						frame.getContentPane().add(lblText);

						frame.pack();
						frame.setVisible(true);

					}
				});
				b9.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						JFrame frame = new JFrame("Calcul statistique");
						frame.setMinimumSize(new Dimension(400, 400));
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						JLabel lblText = new JLabel(
								"Les 10% des classes qui possèdent le plus grand nombre d'attributs : "
										+ parser.getTenPercentOfClassesHaveTheMostAttributes(),
								SwingConstants.CENTER);
						frame.getContentPane().add(lblText);

						frame.pack();
						frame.setVisible(true);

					}
				});
				b10.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						JFrame frame = new JFrame("Calcul statistique");
						frame.setMinimumSize(new Dimension(400, 400));
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						JLabel lblText = new JLabel(
								"Les classes qui font partie en même temps des deux catégories précédentes : "
										+ parser.getCommunElements());
						frame.getContentPane().add(lblText);

						frame.pack();
						frame.setVisible(true);

					}
				});

				b11.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						JFrame frame = new JFrame("Calcul statistique");

						JPanel container = new JPanel();
						final JFormattedTextField x = new JFormattedTextField(NumberFormat.getIntegerInstance());
						JLabel label = new JLabel("Donner le nombre de methode");
						JButton b = new JButton("OK");
						frame.getContentPane().add(label);
						frame.setTitle("Calcul statistique");
						frame.setSize(300, 300);
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						frame.setLocationRelativeTo(null);
						container.setBackground(Color.white);
						container.setLayout(new BorderLayout());
						JPanel top = new JPanel();
						Font police = new Font("Arial", Font.BOLD, 14);
						x.setFont(police);
						x.setPreferredSize(new Dimension(150, 30));
						x.setForeground(Color.BLUE);
						b.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {

								JFrame frame = new JFrame("Calcul statistique");
								frame.setMinimumSize(new Dimension(400, 400));
								frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
								JLabel lblText = new JLabel(
										"Les classes qui possèdent plus de "+x.getText()+" méthodes (la valeur de X est donnée) : "
												+ parser.getClassesThatHaveMoreThenXMethods(
														Integer.parseInt(x.getText())),
										SwingConstants.CENTER);
								frame.getContentPane().add(lblText);

								frame.pack();
								frame.setVisible(true);

							}
						});
						top.add(label);
						top.add(x);
						top.add(b);
						frame.setContentPane(top);
						frame.setVisible(true);

					}
				});

				b12.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {

						JFrame frame = new JFrame("Calcul statistique");
						frame.setMinimumSize(new Dimension(400, 400));
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						JLabel lblText = new JLabel(
								"Les 10% des méthodes qui possèdent le plus grand nombre de lignes de code : "
										+ parser.getTenPercentOfMethodesHaveTheMostCodeLines(),
								SwingConstants.CENTER);
						frame.getContentPane().add(lblText);

						frame.pack();
						frame.setVisible(true);

					}
				});

				b13.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {

						JFrame frame = new JFrame("Calcul statistique");
						frame.setMinimumSize(new Dimension(400, 400));
						frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						JLabel lblText = new JLabel(
								"Le nombre maximal de paramètres par rapport à toutes les méthodes de l’application : "
										+ parser.getNbMaxOfParameter(),
								SwingConstants.CENTER);
						frame.getContentPane().add(lblText);

						frame.pack();
						frame.setVisible(true);

					}
				});

				f.setSize(1700, 500);
				f.setLayout(null);
				f.setVisible(true);
			}

		});

	}

}
