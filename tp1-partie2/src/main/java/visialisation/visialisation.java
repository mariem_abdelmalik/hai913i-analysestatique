package visialisation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.eclipse.jdt.core.dom.SimpleName;

import calculStatistique.Parser;
import grapheAppel.CallGraph;
import grapheAppel.Edge;

public class visialisation {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		System.out.println("Bienvenue dans cette application !  \n");
		
		System.out.println("saisissez votre projectPath");
        Scanner in = new Scanner(System.in);

		final String projectPath = in.next();
				
		System.out.println("saisissez votre jrePath");
		
		final String jrePath = in.next();		
		
    	Parser parser = new Parser(projectPath,jrePath);
    	
    	interaction(parser,projectPath,jrePath);
    	
    	System.out.println("Au revoir");
        	
	
	}
	
	// Question 1.1
    public static void  calculeStatique(Parser parser) {

	//1
    	
	System.out.println("Nombre de classes de l’application : "+ parser.getNbOfClasses()+"\n");
	
	//2
	System.out.println("Nombre de lignes de code de l’application : "+ parser.getNbOfLines()+"\n");
	
	//3
	System.out.println("Nombre de methodes de l’application : "+ parser.getNbOfMethodes()+"\n");

	//4
	System.out.println("Nombre de packages de l’application : "+ parser.getNbOfPackages()+"\n");
	
	//5
	System.out.println("Nombre moyen de méthodes par classe : "+ parser.getNbOfMethodes()/parser.getNbOfClasses()+"\n");

	//6
	System.out.println("Nombre moyen de lignes de code par méthode : "+ parser.getAVGCodeLinesPerMethod()+"\n");

	//7
	System.out.println("Nombre moyen d'attribut par classe : "+ parser.getNbOfAttributes()/parser.getNbOfClasses()+"\n");

	//8
	System.out.println("Les 10% des classes qui possèdent le plus grand nombre de méthodes : "+ parser.getTenPercentOfClassesHaveTheMostMethods()+"\n");

	//9
	System.out.println("Les 10% des classes qui possèdent le plus grand nombre d'attributs : "+ parser.getTenPercentOfClassesHaveTheMostAttributes()+"\n");

	
	//10
	System.out.println("Les classes qui font partie en même temps des deux catégories précédentes : "+parser.getCommunElements()  +"\n");

	//11
	System.out.println("Les classes qui possèdent plus de X méthodes (la valeur de X est donnée) : "+ parser.getClassesThatHaveMoreThenXMethods(2)+"\n");

	//12
	System.out.println("Les 10% des méthodes qui possèdent le plus grand nombre de lignes de code (par classe) : "+ parser.getTenPercentOfMethodesHaveTheMostCodeLines()+"\n");

	//13
	System.out.println("Le nombre maximal de paramètres par rapport à toutes les méthodes de l’application : "+ parser.getNbMaxOfParameter()+"\n");
   
	 
    }
    
  //Graphe d'appel
    public static void  getCallGraph(Parser parser) {
    	
    	CallGraph callGraph = new CallGraph();
  		
  		callGraph.initCallGraph(parser.getCompilationUnits());
  		
  		System.out.println("graph d'appel : \n");
  		
  		for (Edge edge:callGraph.getEdgs()) {
  			
  			System.out.println(edge.getSrc().getName()+  " --> "+edge.getDest().getName());
  			
  		}
    }  
    
    public static void interaction(Parser parser,String projectPath,String jrePath) throws IOException {
        Scanner scanner = new Scanner( System.in ) ;
        
        System.out.print( "Veuillez saisir 1 pour avoir le calcul statistique \n" );
		System.out.print( "Veuillez saisir 2 pour avoir  le graphe d'appel \n" );
		System.out.print( "Veuillez saisir 3 pour avoir  l'interface graphique \n" );
		System.out.print( "Veuillez saisir n'import quelle autre caractère pour quitter  \n" );
		int a = scanner.nextInt();

		if (a==1) {
			calculeStatique(parser);
			interaction(parser,projectPath, jrePath);
		}
		
		if (a==2) {
			getCallGraph(parser);
			interaction(parser,projectPath, jrePath);
		}
		if (a==3) {
			Processor processor = new Processor();
			processor.graphiqueInterface(projectPath, jrePath);
			
		}
    }

	private static void calculeStatique() {
		// TODO Auto-generated method stub
		
	}

}
