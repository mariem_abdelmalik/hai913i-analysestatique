package calculStatistique;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.jar.Attributes.Name;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.internal.utils.FileUtil;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import visitor.FieldDeclarationVisitor;
import visitor.MethodDeclarationVisitor;
import visitor.PackageDeclarationVisitor;
import visitor.TypeDeclarationVisitor;
import visitor.VariableDeclarationFragmentVisitor;

public class Parser {
	
	private String projectSource;
	private String jrePath;
	private int nbOfClasses;
	private int nbOfLines;
	private int nbOfMethodes;
	private int nbOfPackages;
	private int nbOfAttributes;
	private Map<TypeDeclaration,List<MethodDeclaration>> methodsOfClass;
	private Map<MethodDeclaration,Integer> nblinesOfMethode;
	private Map<TypeDeclaration,List<VariableDeclarationFragment>> attributesOfClass;
	private List<SimpleName> TenPercentOfClassesHaveTheMostMethods;
	private List<SimpleName> TenPercentOfClassesHaveTheMostAttributes;
	private List<SimpleName> TenPercentOfMethodesHaveTheMostCodeLines;
	private Double AVGCodeLinesPerMethod;
	private int nbMaxOfParameter;
	private ArrayList<CompilationUnit> compilationUnits = new ArrayList<CompilationUnit>();


	public Parser (String projectSource,String jrePath) throws IOException{
		
		this.setJrePath(jrePath);
		this.setProjectSource(projectSource);
		String projectSourcePath = projectSource + "/src";
		final File folder = new File(projectSourcePath);
		ArrayList<File> javaFiles = listJavaFilesForFolder(folder);
		this.compilationUnits= this.parse(javaFiles);
		this.initNbOfClasses();
		this.initNblinesOfMethode();
		this.initNbOfMethodesAndMethodsOfClass();
		this.initNbOfPackages();
		this.initNbOfAttributeAndAttributesOfClass();
		this.initTenPercentOfClassesHaveTheMostOfMethods();
		this.initTenPercentOfClassesHaveTheMostOfAttributes();
		this.initAVGCodeLinesPerMethod();
		this.initTenPercentOfMethodesHaveTheMostCodeLines();
		this.initNbMaxOfParameter();
		
	}
	
	// initialisation de NbOfClasses en se basant sur compilationUnits
	public void initNbOfClasses() throws IOException {
		
		int nbOfClasses = 0;
		
		for (CompilationUnit parse : this.compilationUnits) {
			
			TypeDeclarationVisitor visitor = new TypeDeclarationVisitor();
			
			parse.accept(visitor);
			
			for (TypeDeclaration type :visitor.getTypes() ) {
				
				if (!type.isInterface()) {
					
					nbOfClasses = nbOfClasses + 1;
					
				}
			}
		}		
		
		this.nbOfClasses=nbOfClasses;
	}
	
	// recherche de nombre de methode et les methodes associés a chaque classe

		public void initNbOfMethodesAndMethodsOfClass() throws IOException {
			
			int nbOfMethodes = 0;
			
			this.methodsOfClass = new HashMap<TypeDeclaration, List<MethodDeclaration>>() ;
			
			 for (CompilationUnit parse : this.compilationUnits) {
				 
				TypeDeclarationVisitor visitor = new TypeDeclarationVisitor();
				
				parse.accept(visitor);

				for (TypeDeclaration type : visitor.getTypes()) {
					
					if (!type.isInterface()) {
											
						List<MethodDeclaration> methodDeclarations = new ArrayList<MethodDeclaration>();

						for (MethodDeclaration methodDeclaration : type.getMethods()) {

							methodDeclarations.add(methodDeclaration);
							
							nbOfMethodes++;
							
						}
						
						methodsOfClass.put(type, methodDeclarations);
						
						}
				}
			}	
			
			this.nbOfMethodes=nbOfMethodes;
		}

		// initialisation de nb de packages
		public void initNbOfPackages() throws IOException {
					
			HashSet<String> packages = new HashSet<String>();
			
			this.attributesOfClass = new HashMap<TypeDeclaration, List<VariableDeclarationFragment>>() ;
			
			for (CompilationUnit parse : this.compilationUnits) {
				
				PackageDeclarationVisitor visitor = new PackageDeclarationVisitor();
				
				parse.accept(visitor);
				
				for (PackageDeclaration packageDeclaration : visitor.packages) {
					
					packages.add((packageDeclaration.getName().toString()));
				
				}
			}
						 
			this.nbOfPackages=packages.size();
		}
		
		
		// calcule de la moyenne de ligne de code par methode
		public void initAVGCodeLinesPerMethod() {
			
			this.AVGCodeLinesPerMethod = (double) 0;
			
			for (Entry<MethodDeclaration, Integer> entry : this.nblinesOfMethode.entrySet()) {
				
				this.AVGCodeLinesPerMethod = this.AVGCodeLinesPerMethod + entry.getValue();
				
			}
			
			this.AVGCodeLinesPerMethod = this.AVGCodeLinesPerMethod/this.nbOfMethodes;
			
		}
		
	//pour chaque methode on lui associe son nombre de ligne de code 
	public void initNblinesOfMethode() {
		
		this.nblinesOfMethode = new HashMap<MethodDeclaration, Integer>() ;

		 for (CompilationUnit parse : this.compilationUnits) {
				
			MethodDeclarationVisitor visitor = new MethodDeclarationVisitor();
			
			parse.accept(visitor);

			for (MethodDeclaration methodDeclaration : visitor.getMethods()) {
				
				int startLineNum = parse.getLineNumber(methodDeclaration.getStartPosition());
				
				int endLineNum = parse.getLineNumber(methodDeclaration.getStartPosition() + methodDeclaration.getLength());
				
				this.nblinesOfMethode.put(methodDeclaration, endLineNum-startLineNum+1);
				
			}
					
		}
		 
	}
		
	
	//initialisation de nombre d'attribut et les attribut associer à chaque class
	public  void initNbOfAttributeAndAttributesOfClass() throws IOException {
		
		int nbOfAttributes = 0;
		
		for (CompilationUnit parse : this.compilationUnits) {
			
			TypeDeclarationVisitor visitor = new TypeDeclarationVisitor();
			
			parse.accept(visitor);

			for (TypeDeclaration type : visitor.getTypes()) {
				
				if (!type.isInterface()) {
					
					FieldDeclarationVisitor visitor2 = new FieldDeclarationVisitor();
					
					new VariableDeclarationFragmentVisitor();
					
					type.accept(visitor2);
					
					List<VariableDeclarationFragment> variableDeclarationFragment = new ArrayList<VariableDeclarationFragment>();
					
					for (FieldDeclaration FieldDeclaration : visitor2.fields) {
						
						variableDeclarationFragment = FieldDeclaration.fragments();
						
						attributesOfClass.put(type, variableDeclarationFragment);
						
						nbOfAttributes = nbOfAttributes + variableDeclarationFragment.size();	
					}
										}
			}
		}
					 
		this.nbOfAttributes= nbOfAttributes;
	}
	
	
	//initialisation de TenPercentOfClassesHaveTheMostOfMethods  
	public void initTenPercentOfClassesHaveTheMostOfMethods() throws IOException {
		
		List<TypeDeclaration> listOfClasses = new ArrayList<TypeDeclaration>();
		
		for (Entry<TypeDeclaration, List<MethodDeclaration>> entry : this.methodsOfClass.entrySet()) {
			
			listOfClasses.add(entry.getKey());
			
		}

		TypeDeclaration typeDeclaration;
		
		for (int i=0;i<listOfClasses.size()-1;i++) {
			
			for (int j=i+1;j<listOfClasses.size();j++) {
				
				if (this.methodsOfClass.get(listOfClasses.get(i)).size()>this.methodsOfClass.get(listOfClasses.get(j)).size()) {
					
					typeDeclaration = listOfClasses.get(j);
					
					listOfClasses.set(j, listOfClasses.get(i));
					
					listOfClasses.set(i, typeDeclaration);
				}
			}
		}
		
		int tenPercentOfClasses = this.nbOfClasses/10 + 1;
		
		this.TenPercentOfClassesHaveTheMostMethods = new ArrayList<SimpleName>();
		
		for (int i = 0;i<tenPercentOfClasses;i++ ) {
			
			this.TenPercentOfClassesHaveTheMostMethods.add(listOfClasses.get(i).getName());
			
		}
	}
	
	//initialisation de TenPercentOfClassesHaveTheMostOfAttributes  

	public void initTenPercentOfClassesHaveTheMostOfAttributes() throws IOException {
		
		List<TypeDeclaration> listOfClasses = new ArrayList<TypeDeclaration>();

		for (TypeDeclaration TypeDeclaration :this.attributesOfClass.keySet() ) {
			
			listOfClasses.add(TypeDeclaration);
			
		}
		
		TypeDeclaration typeDeclaration;
		
		for (int i=0;i<listOfClasses.size()-1;i++) {
			
			for (int j=i+1;j<listOfClasses.size();j++) {
				
				if (this.attributesOfClass.get(listOfClasses.get(i)).size()>this.attributesOfClass.get(listOfClasses.get(j)).size()) {
					
					typeDeclaration = listOfClasses.get(j);
					
					listOfClasses.set(j, listOfClasses.get(i));
					
					listOfClasses.set(i, typeDeclaration);
					
				}
			}
		}
		
		int tenPercentOfClasses = this.nbOfClasses/10 + 1;
		
		this.TenPercentOfClassesHaveTheMostAttributes =  new ArrayList<SimpleName>();
		
		for (int i=0;i<tenPercentOfClasses;i++) {
			
			this.TenPercentOfClassesHaveTheMostAttributes.add(listOfClasses.get(i).getName());
		}
		
	}
	
	

	//recherche des classes qui font partie en meme temps des deux catégories précedentes.
	public List<SimpleName> getCommunElements(){
		
		List<SimpleName> l = new ArrayList<SimpleName>();
		
		l=this.TenPercentOfClassesHaveTheMostMethods;
		
		l.retainAll(this.TenPercentOfClassesHaveTheMostAttributes);
		
		return l;
	}
	
	//recherche des classes qui possèdent plus de X méthodes
	public List<SimpleName> getClassesThatHaveMoreThenXMethods(int x){
		
		List<SimpleName> classesThatHaveMoreThenXMethods = new ArrayList<SimpleName>();
		
		for (TypeDeclaration typeDeclaration : this.methodsOfClass.keySet()) {
			
			if (this.methodsOfClass.get(typeDeclaration).size()>x) {
				
				classesThatHaveMoreThenXMethods.add(typeDeclaration.getName());
				
			}
		}
		
		return classesThatHaveMoreThenXMethods;
		
	}
	
	//Les 10% des méthodes qui possèdent le plus grand nombre de lignes de code (par classe).
	public void initTenPercentOfMethodesHaveTheMostCodeLines() throws IOException {
		
		List<MethodDeclaration> listOfMethods = new ArrayList<MethodDeclaration>();
		
		for (Entry<TypeDeclaration, List<MethodDeclaration>> entry : this.methodsOfClass.entrySet()) {
			
			for (MethodDeclaration methodDeclaration : entry.getValue()) {
				
				listOfMethods.add(methodDeclaration);
			}
			
		}
		
		MethodDeclaration methodDeclaration;
		
		for (int i=0;i<listOfMethods.size()-1;i++) {
			
			for (int j=i+1;j<listOfMethods.size();j++) {
								
				if (this.nblinesOfMethode.get((listOfMethods.get(i))) >this.nblinesOfMethode.get(listOfMethods.get(j))) {
					
					methodDeclaration = (MethodDeclaration) listOfMethods.get(j);
					
					listOfMethods.set(j, listOfMethods.get(i));
					
					listOfMethods.set(i, methodDeclaration);
					
				}
			}
		}
		
		int tenPercentOfClasses = this.nbOfClasses/10 + 1;
				
		this.TenPercentOfMethodesHaveTheMostCodeLines = new ArrayList<SimpleName>();
		
		for (int i=0;i<tenPercentOfClasses;i++) {
			
			this.TenPercentOfMethodesHaveTheMostCodeLines.add(listOfMethods.get(i).getName());
		}
		
	}
	
	//Le nombre maximal de paramètres par rapport à toutes les méthodes de l’application
	public void initNbMaxOfParameter() {
		
		this.nbMaxOfParameter = 0;
		
		for (Entry<TypeDeclaration, List<MethodDeclaration>> entry : this.methodsOfClass.entrySet()) {
			
			for (MethodDeclaration methodDeclaration : entry.getValue()) {
				
				if (nbMaxOfParameter < methodDeclaration.parameters().size()) {
					
					this.nbMaxOfParameter = methodDeclaration.parameters().size();
					
				}			
			}
			
		}

	}
	
	private ArrayList<CompilationUnit> parse(ArrayList<File> javaFiles) throws IOException {
		
		String projectSourcePath = this.projectSource + "/src";
		
		String[] sources = { projectSourcePath }; 
		
		String[] classpath = {jrePath};
		
		ASTParser parser = ASTParser.newParser(AST.JLS4); // java +1.6
		
		parser.setResolveBindings(true);
		
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		
		parser.setBindingsRecovery(true);
		
		Map options = JavaCore.getOptions();
		
		parser.setCompilerOptions(options);
		
		parser.setUnitName("");

		for (File fileEntry : javaFiles) {
			
				String content = FileUtils.readFileToString(fileEntry);
				
				parser.setEnvironment(classpath,sources, new String[] { "UTF-8"}, true);
				
				parser.setSource(content.toCharArray());
				
				nbOfLines = (int) (nbOfLines +content.lines().count());
				
				CompilationUnit compilationUnit = (CompilationUnit) parser.createAST(null); // create and parse
				
				compilationUnits.add(compilationUnit);
				
				}
		
		return compilationUnits;
		
	}
	
	public static ArrayList<File> listJavaFilesForFolder(final File folder) {
		
		ArrayList<File> javaFiles = new ArrayList<File>();
		
		for (File fileEntry : folder.listFiles()) {
			
			if (fileEntry.isDirectory()) {
				
				javaFiles.addAll(listJavaFilesForFolder(fileEntry));
				
			} else if (fileEntry.getName().contains(".java")) {
				
				javaFiles.add(fileEntry);
				
			}
		}

		return javaFiles;
	}

	public ArrayList<CompilationUnit> getCompilationUnits() {
		return compilationUnits;
	}

	public void setCompilationUnits(ArrayList<CompilationUnit> compilationUnits) {
		this.compilationUnits = compilationUnits;
	}

	public String getProjectSource() {
		return projectSource;
	}

	public void setProjectSource(String projectSource) {
		this.projectSource = projectSource;
	}

	public String getJrePath() {
		return jrePath;
	}

	public void setJrePath(String jrePath) {
		this.jrePath = jrePath;
	}

	public int getNbOfClasses() {
		return nbOfClasses;
	}

	public void setNbOfClasses(int nbOfClasses) {
		this.nbOfClasses = nbOfClasses;
	}

	public int getNbOfLines() {
		return nbOfLines;
	}

	public void setNbOfLines(int nbOfLines) {
		this.nbOfLines = nbOfLines;
	}

	public int getNbOfMethodes() {
		return nbOfMethodes;
	}

	public void setNbOfMethodes(int nbOfMethodes) {
		this.nbOfMethodes = nbOfMethodes;
	}

	public int getNbOfPackages() {
		return nbOfPackages;
	}


	public void setNbOfPackages(int nbOfPackages) {
		this.nbOfPackages = nbOfPackages;
	}


	public int getNbOfAttributes() {
		return nbOfAttributes;
	}


	public void setNbOfAttributes(int nbOfAttributes) {
		this.nbOfAttributes = nbOfAttributes;
	}

	public Map<TypeDeclaration, List<MethodDeclaration>> getMethodsOfClass() {
		return methodsOfClass;
	}

	public void setMethodsOfClass(Map<TypeDeclaration, List<MethodDeclaration>> methodsOfClass) {
		this.methodsOfClass = methodsOfClass;
	}

	public Map<TypeDeclaration, List<VariableDeclarationFragment>> getAttributesOfClass() {
		return attributesOfClass;
	}

	public void setAttributesOfClass(Map<TypeDeclaration, List<VariableDeclarationFragment>> attributesOfClass) {
		this.attributesOfClass = attributesOfClass;
	}

	public List<SimpleName> getTenPercentOfClassesHaveTheMostMethods() {
		return TenPercentOfClassesHaveTheMostMethods;
	}


	public void setTenPercentOfClassesHaveTheMostMethods(List<SimpleName> tenPercentOfClassesHaveTheMostMethods) {
		TenPercentOfClassesHaveTheMostMethods = tenPercentOfClassesHaveTheMostMethods;
	}


	public List<SimpleName> getTenPercentOfClassesHaveTheMostAttributes() {
		return TenPercentOfClassesHaveTheMostAttributes;
	}


	public void setTenPercentOfClassesHaveTheMostAttributes(List<SimpleName> tenPercentOfClassesHaveTheMostAttributes) {
		TenPercentOfClassesHaveTheMostAttributes = tenPercentOfClassesHaveTheMostAttributes;
	}


	public Map<MethodDeclaration, Integer> getNblinesOfMethode() {
		return nblinesOfMethode;
	}


	public void setNblinesOfMethode(Map<MethodDeclaration, Integer> nblinesOfMethode) {
		this.nblinesOfMethode = nblinesOfMethode;
	}



	public List<SimpleName> getTenPercentOfMethodesHaveTheMostCodeLines() {
		return TenPercentOfMethodesHaveTheMostCodeLines;
	}


	public void setTenPercentOfMethodesHaveTheMostCodeLines(List<SimpleName> tenPercentOfMethodesHaveTheMostCodeLines) {
		TenPercentOfMethodesHaveTheMostCodeLines = tenPercentOfMethodesHaveTheMostCodeLines;
	}


	public Double getAVGCodeLinesPerMethod() {
		return AVGCodeLinesPerMethod;
	}


	public void setAVGCodeLinesPerMethod(Double aVGCodeLinesPerMethod) {
		AVGCodeLinesPerMethod = aVGCodeLinesPerMethod;
	}


	public int getNbMaxOfParameter() {
		return nbMaxOfParameter;
	}


	public void setNbMaxOfParameter(int nbMaxOfParameter) {
		this.nbMaxOfParameter = nbMaxOfParameter;
	}
	
	
}
